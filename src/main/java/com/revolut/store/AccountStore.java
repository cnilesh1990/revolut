package com.revolut.store;

import com.revolut.model.Account;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class AccountStore {

    private static final Map<String, Account> ACCOUNT_MAP = new ConcurrentHashMap<>();

    public static Map<String, Account> getAccountMap() {
        return ACCOUNT_MAP;
    }
}
