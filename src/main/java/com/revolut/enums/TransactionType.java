package com.revolut.enums;

/**
 * The type of transaction which the user wants to initiate
 */
public enum TransactionType {
    WITHDRAW,DEPOSIT,TRANSFER;
}
