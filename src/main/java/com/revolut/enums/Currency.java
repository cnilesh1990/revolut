package com.revolut.enums;

/**
 * The currency which the bank supports
 */
public enum Currency {
    GBP, USD, INR, EUR;
}
