package com.revolut.launch;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

import java.io.IOException;

/**
 * Using a custom Jersey embedded server
 */

@SuppressWarnings("restriction")
public class JerseyEmbeddedHTTPServer {

    public static void main(String[] args) throws IOException {
        Server server = new Server(8080);

        ServletContextHandler ctx =
                new ServletContextHandler(ServletContextHandler.NO_SESSIONS);

        ctx.setContextPath("/");
        server.setHandler(ctx);

        ServletHolder serHol = ctx.addServlet(ServletContainer.class, "/revolut/*");
        serHol.setInitOrder(1);
        serHol.setInitParameter("jersey.config.server.provider.packages",
                "com.revolut.controller");

        try {
            server.start();
            server.join();
        } catch (Exception ex) {
            System.err.println(ex);
        } finally {

            server.destroy();
        }
    }
}