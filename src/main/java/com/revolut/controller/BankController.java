package com.revolut.controller;

import com.revolut.exception.InsufficientBalanceException;
import com.revolut.exception.InvalidAccountException;
import com.revolut.model.Customer;
import com.revolut.model.Transaction;
import com.revolut.model.TransactionResponse;
import com.revolut.proxy.BankServiceInitializer;
import com.revolut.service.BankService;
import com.revolut.service.BankServiceImpl;
import org.apache.commons.httpclient.HttpStatus;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("revolut")
public class BankController {

    @POST
    @Path("/createAccount")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TransactionResponse createAccount(Customer customerName) {
        TransactionResponse response = new TransactionResponse();
        try {
            BankService service = BankServiceInitializer.newInstance(new BankServiceImpl());
            String accountNumber = service.createAccount(customerName.getName());
            response.setStatus(HttpStatus.SC_CREATED);
            response.setMessage(String.format("Account created with account Number %s", accountNumber));
        } catch (Exception e) {
            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.setMessage(e.getMessage());
        }
        return response;
    }

    @POST
    @Path("/deposit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TransactionResponse deposit(Transaction transaction) {
        TransactionResponse response = new TransactionResponse();
        try {
            transaction.setFromAccount(null);
            BankService service = BankServiceInitializer.newInstance(new BankServiceImpl());
            service.deposit(transaction);
            response.setStatus(HttpStatus.SC_OK);
        } catch (InvalidAccountException | InsufficientBalanceException e) {
            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.setMessage(e.getMessage());
        } catch(Exception e) {
            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.setMessage("Some issue occurred");
        }
        return response;
    }

    @POST
    @Path("/withdraw")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TransactionResponse withdraw(Transaction transaction) {
        TransactionResponse response = new TransactionResponse();
        try {
            BankService service = BankServiceInitializer.newInstance(new BankServiceImpl());
            transaction.setToAccount(null);
            service.withdraw(transaction);
            response.setStatus(HttpStatus.SC_OK);
        } catch (InvalidAccountException | InsufficientBalanceException e) {
            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.setMessage(e.getMessage());
        } catch(Exception e) {
            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.setMessage("Some issue occurred");
        }
        return response;
    }

    @POST
    @Path("/transfer")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TransactionResponse transfer(Transaction transaction) {
        TransactionResponse response = new TransactionResponse();
        try {
            BankService service = BankServiceInitializer.newInstance(new BankServiceImpl());
            service.transfer(transaction);
            response.setStatus(HttpStatus.SC_OK);
        } catch (InvalidAccountException | InsufficientBalanceException e) {
            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.setMessage(e.getMessage());
        } catch(Exception e) {
            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.setMessage("Some issue occurred");
        }
        return response;
    }
}
