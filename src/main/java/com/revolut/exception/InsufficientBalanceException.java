package com.revolut.exception;

/**
 * Custom exception thrown in case the user wants to transfer
 * or withdraw money more than their balance
 */
public class InsufficientBalanceException extends Exception {
    public InsufficientBalanceException(String message) {
        super(message);
    }
}
