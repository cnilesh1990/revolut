package com.revolut.exception;

/**
 * Custom exception thrown in case the amount entered
 * to execute the transaction is invalid
 */
public class InvalidAmountException extends Exception {
    public InvalidAmountException(String message) {
        super(message);
    }
}
