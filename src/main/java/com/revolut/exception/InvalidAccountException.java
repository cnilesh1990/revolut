package com.revolut.exception;

/**
 * Custom exception thrown in case the account
 * number entered is invalid.
 */
public class InvalidAccountException extends Exception {
    public InvalidAccountException(String message) {
        super(message);
    }
}
