package com.revolut.model;

import com.revolut.enums.Currency;

import java.math.BigDecimal;

public class Money {

    private BigDecimal value;

    private Currency currency = Currency.GBP;

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
