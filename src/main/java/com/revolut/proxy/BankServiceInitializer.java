package com.revolut.proxy;

import com.revolut.exception.InsufficientBalanceException;
import com.revolut.exception.InvalidAccountException;
import com.revolut.exception.InvalidAmountException;
import com.revolut.model.Transaction;
import com.revolut.service.BankService;
import com.revolut.service.BankServiceImpl;
import com.revolut.validation.Validate;
import com.revolut.validation.Validator;
import com.revolut.validation.ValidatorImpl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BankServiceInitializer implements InvocationHandler {

    private Validator validator;
    private BankService bankService;

    private BankServiceInitializer(BankService bankService, Validator validator) {
        this.bankService = bankService;
        this.validator = validator;
    }

    /**
     * @param bankService
     * @return
     *
     * Initializes a bank service object. The motivation behind this is
     * AOP. The business logic should be different from the validation logic.
     * This ensures that validation is automatically invoked for a transaction.
     * If the validation is successful, only then that method is invoked.
     */
    public static BankService newInstance(BankService bankService) {
        return (BankService) java.lang.reflect.Proxy.newProxyInstance(bankService.getClass().getClassLoader(),
                bankService.getClass().getInterfaces(), new BankServiceInitializer(bankService, new ValidatorImpl()));
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable, InvalidAccountException, InsufficientBalanceException, InvalidAmountException {
        boolean result = true;
        Object res = null;
        try {
            if(method.isAnnotationPresent(Validate.class)) {            //Check if method is annotated with @Validate
                validator.validateTransaction((Transaction) args[0]);   //Invoke validation if annoted with @Validate
            }
            if(result) {
                res = method.invoke(bankService, args);                 //If validation is successful, invoke the target method
            }
        } catch (InvalidAccountException | InsufficientBalanceException | InvalidAmountException e) {
            throw e;                                                    //Propogate the error if any validation issues.
        } catch (Exception e) {
            throw new RuntimeException("unexpected invocation exception: " + e.getMessage());
        }
        return res;
    }
}
