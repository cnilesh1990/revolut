package com.revolut.validation;

import com.revolut.exception.InsufficientBalanceException;
import com.revolut.exception.InvalidAccountException;
import com.revolut.exception.InvalidAmountException;
import com.revolut.model.Transaction;

public interface Validator {

    void validateTransaction(Transaction transaction) throws InsufficientBalanceException, InvalidAccountException, InvalidAmountException;

}
