package com.revolut.validation;

import com.revolut.exception.InsufficientBalanceException;
import com.revolut.exception.InvalidAccountException;
import com.revolut.model.Account;
import com.revolut.model.Transaction;
import com.revolut.store.AccountStore;

import java.math.BigDecimal;

public class WithdrawalValidator implements Validator {
    @Override
    public void validateTransaction(Transaction transaction) throws InsufficientBalanceException, InvalidAccountException {
        if(transaction.getAmount().getValue().compareTo(BigDecimal.ZERO) <= 0) {
            throw new InsufficientBalanceException("Please enter a valid amount to continue");
        }
        if(transaction.getFromAccount() == null) {
            throw new InvalidAccountException("Please enter a valid account number");
        }
        Account account = AccountStore.getAccountMap().get(transaction.getFromAccount());
        if(account == null) {
            throw new InvalidAccountException("Please enter a valid account number");
        }
        if(account.getBalance().compareTo(transaction.getAmount().getValue()) < 0) {
            throw new InsufficientBalanceException("Insufficient Funds");
        }
    }
}
