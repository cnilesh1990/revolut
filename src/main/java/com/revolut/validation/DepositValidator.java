package com.revolut.validation;

import com.revolut.exception.InvalidAccountException;
import com.revolut.exception.InvalidAmountException;
import com.revolut.model.Account;
import com.revolut.model.Transaction;
import com.revolut.store.AccountStore;

import java.math.BigDecimal;

public class DepositValidator implements Validator {
    /**
     * All validations related to deposit are mentioned here.
     * @param transaction
     * @throws InvalidAccountException
     * @throws InvalidAmountException
     */
    @Override
    public void validateTransaction(Transaction transaction) throws InvalidAccountException, InvalidAmountException {
        if(transaction.getAmount().getValue().compareTo(BigDecimal.ZERO) <= 0) {        //Check if amount specified is 0
            throw new InvalidAmountException("Please enter a valid amount to deposit!");
        }
        if(transaction.getToAccount() == null) {                                        //Check if account specified is null
            throw new InvalidAccountException("Please enter a valid account number");
        }
        Account account = AccountStore.getAccountMap().get(transaction.getToAccount());
        if(account == null) {                                                           //Check for existence of account.
            throw new InvalidAccountException("Please enter a valid account number");
        }
    }
}
