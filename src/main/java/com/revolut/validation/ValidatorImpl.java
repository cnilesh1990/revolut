package com.revolut.validation;

import com.revolut.enums.TransactionType;
import com.revolut.exception.InsufficientBalanceException;
import com.revolut.exception.InvalidAccountException;
import com.revolut.exception.InvalidAmountException;
import com.revolut.model.Transaction;

public class ValidatorImpl implements Validator {
    @Override
    public void validateTransaction(Transaction transaction) throws InsufficientBalanceException, InvalidAccountException, InvalidAmountException {
        Validator validator;
        if(TransactionType.WITHDRAW.equals(transaction.getTransactionType()) ) {
            validator = new WithdrawalValidator();
            validator.validateTransaction(transaction);
        } else if(TransactionType.DEPOSIT.equals(transaction.getTransactionType())) {
            validator = new DepositValidator();
            validator.validateTransaction(transaction);
        } else if(TransactionType.TRANSFER.equals(transaction.getTransactionType())){
          validator = new TransferValidator();
          validator.validateTransaction(transaction);
        }
    }
}
