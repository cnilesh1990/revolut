package com.revolut.validation;

import com.revolut.exception.InsufficientBalanceException;
import com.revolut.exception.InvalidAccountException;
import com.revolut.exception.InvalidAmountException;
import com.revolut.model.Account;
import com.revolut.model.Transaction;
import com.revolut.store.AccountStore;

import java.math.BigDecimal;

public class TransferValidator implements Validator {
    @Override
    public void validateTransaction(Transaction transaction) throws InvalidAccountException, InsufficientBalanceException, InvalidAmountException {
        if(transaction.getFromAccount() == null || transaction.getToAccount() == null) {
            throw new InvalidAccountException("Invalid account. Please check the account number");
        }
        if(transaction.getFromAccount().equals(transaction.getToAccount())) {
            throw new InvalidAccountException("From and to accounts cannot be same");
        }
        if(transaction.getAmount().getValue().compareTo(BigDecimal.ZERO) == 0) {
            throw new InvalidAmountException("Please enter a valid amount to transfer");
        }
        Account fromAccount = AccountStore.getAccountMap().get(transaction.getFromAccount());
        if(fromAccount.getBalance().compareTo(transaction.getAmount().getValue()) < 0) {
            throw new InsufficientBalanceException("Insufficient funds");
        }
        Account toAccount = AccountStore.getAccountMap().get(transaction.getToAccount());
        if(toAccount == null) {
            throw new InvalidAccountException("Please enter a valid recipient account number");
        }
    }
}
