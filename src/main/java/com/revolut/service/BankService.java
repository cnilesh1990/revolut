package com.revolut.service;

import com.revolut.exception.InsufficientBalanceException;
import com.revolut.exception.InvalidAccountException;
import com.revolut.exception.InvalidAmountException;
import com.revolut.model.Transaction;
import com.revolut.validation.Validate;

public interface BankService {
    @Validate
    void deposit(Transaction transaction) throws InvalidAccountException, InsufficientBalanceException, InvalidAmountException;

    @Validate
    void withdraw(Transaction transaction) throws InvalidAccountException, InsufficientBalanceException, InvalidAmountException;

    @Validate
    void transfer(Transaction transaction) throws InvalidAccountException, InsufficientBalanceException, InvalidAmountException;

    String createAccount(String customerName);
}
