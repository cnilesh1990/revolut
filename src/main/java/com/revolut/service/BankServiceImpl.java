package com.revolut.service;

import com.revolut.exception.InsufficientBalanceException;
import com.revolut.exception.InvalidAccountException;
import com.revolut.exception.InvalidAmountException;
import com.revolut.model.Account;
import com.revolut.model.Customer;
import com.revolut.model.Transaction;
import com.revolut.store.AccountStore;
import com.revolut.validation.Validate;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;

public class BankServiceImpl implements BankService {

    private static BigInteger accountNumber = BigInteger.ONE;

    /**
     *
     * @param transaction
     * @throws InvalidAccountException
     * @throws InsufficientBalanceException
     * @throws InvalidAmountException
     * Deposits the user amount and adds the amount to their balance
     * Throws an exception if amount to be deposited in 0
     * Throws an exception if account does not exists or null
     * Since we deposit an ammount TO a user, so toAccount is to be used.
     */
    @Override
    @Validate
    public void deposit(Transaction transaction)  throws InvalidAccountException, InsufficientBalanceException, InvalidAmountException {
        Account account = AccountStore.getAccountMap().get(transaction.getToAccount());
        synchronized (account) {
            account.setBalance(transaction.getAmount().getValue().add(account.getBalance()));
            AccountStore.getAccountMap().put(account.getAccountNumber(), account);
        }
    }

    /**
     * @param transaction
     * @throws InvalidAccountException
     * @throws InsufficientBalanceException
     * @throws InvalidAmountException
     * Since we withdraw mooney FROM a user account, fromAccount is used.
     * Withdraws money from user account and reduces the balance.
     * Throws exception is amount specified is 0, or invalid account is specified.
     */
    @Override
    @Validate
    public void withdraw(Transaction transaction)  throws InvalidAccountException, InsufficientBalanceException, InvalidAmountException {
        Account account = AccountStore.getAccountMap().get(transaction.getFromAccount());
        synchronized (account) {
            account.setBalance(account.getBalance().subtract(transaction.getAmount().getValue()));
            AccountStore.getAccountMap().put(account.getAccountNumber(), account);
        }
    }

    /**
     * @param transaction
     * @throws InvalidAccountException
     * @throws InsufficientBalanceException
     * @throws InvalidAmountException
     * Transfer money from account to to Account.
     * Exception if from account or to account are same, or if either of the accounts are invalid.
     * Exception if both the accounts are same, or amount specified is 0.
     */
    @Override
    @Validate
    public void transfer(Transaction transaction)  throws InvalidAccountException, InsufficientBalanceException, InvalidAmountException {
        Account fromAccount = AccountStore.getAccountMap().get(transaction.getFromAccount());
        Account toAccount = AccountStore.getAccountMap().get(transaction.getToAccount());
        synchronized (fromAccount) {
            synchronized (toAccount) {
                fromAccount.setBalance(fromAccount.getBalance().subtract(transaction.getAmount().getValue()));
                toAccount.setBalance(toAccount.getBalance().add(transaction.getAmount().getValue()));
                AccountStore.getAccountMap().put(fromAccount.getAccountNumber(), fromAccount);
                AccountStore.getAccountMap().put(toAccount.getAccountNumber(), toAccount);
            }
        }
    }

    /**
     * Create a user account.
     * @param customerName
     * @return
     */
    @Override
    public String createAccount(String customerName) {
        synchronized (Customer.class) {
            Customer customer = new Customer();
            customer.setName(customerName);
            Account account = new Account();
            account.setAccountNumber(String.valueOf(accountNumber));
            account.setBalance(BigDecimal.ZERO);
            account.setCustomer(customer);
            customer.setAccounts(new HashSet<Account>(Arrays.asList(account)));
            AccountStore.getAccountMap().put(String.valueOf(accountNumber), account);
            accountNumber = accountNumber.add(BigInteger.ONE);
            return String.valueOf(accountNumber.subtract(BigInteger.ONE));
        }
    }
}
