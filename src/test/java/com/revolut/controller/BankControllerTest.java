package com.revolut.controller;

import io.restassured.RestAssured;
import io.restassured.internal.path.json.mapping.JsonObjectDeserializer;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;

public class BankControllerTest {

    @Test
    public void testCreateAccount() {
        String name = "Nilesh";
        RestAssured.baseURI ="http://localhost:8080";
        RequestSpecification request = RestAssured.given();
        JSONObject requestParams = new JSONObject();
        requestParams.put("name", "revolut");
        request.header("Content-Type", "application/json");
        request.body(requestParams.toString());
        Response response = request.post("/revolut/createAccount");
        int statusCode = response.getStatusCode();
        Assert.assertEquals( 200, statusCode);
        String message = response.jsonPath().get("message");
        Assert.assertEquals("Account created with account Number", message.substring(0, message.length() - 2).trim());
    }

    @Test
    public void testDeposit() {
        String name = "Nilesh";
        RestAssured.baseURI ="http://localhost:8080";
        RequestSpecification request = RestAssured.given();
        JSONObject requestParams = new JSONObject();
        JSONObject value = new JSONObject();
        value.put("value", "100");
        value.put("currency", "GBP");
        requestParams.put("toAccount", "1");
        requestParams.put("amount", value);
        requestParams.put("transactionType", "DEPOSIT");
        request.header("Content-Type", "application/json");
        request.body(requestParams.toString());
        Response response = request.post("/revolut/deposit");
        int statusCode = response.getStatusCode();
        Assert.assertEquals( 200, statusCode);
    }

    @Test
    public void testWithdraw() {
        String name = "Nilesh";
        RestAssured.baseURI ="http://localhost:8080";
        RequestSpecification request = RestAssured.given();
        JSONObject requestParams = new JSONObject();
        JSONObject value = new JSONObject();
        value.put("value", "100");
        value.put("currency", "GBP");
        requestParams.put("toAccount", "1");
        requestParams.put("amount", value);
        requestParams.put("transactionType", "WITHDRAW");
        request.header("Content-Type", "application/json");
        request.body(requestParams.toString());
        Response response = request.post("/revolut/withdraw");
        int statusCode = response.getStatusCode();
        Assert.assertEquals( 200, statusCode);
    }

    @Test
    public void testTransfer() {
        String name = "Nilesh";
        RestAssured.baseURI ="http://localhost:8080";
        RequestSpecification request = RestAssured.given();
        JSONObject requestParams = new JSONObject();
        JSONObject value = new JSONObject();
        value.put("value", "100");
        value.put("currency", "GBP");
        requestParams.put("toAccount", "1");
        requestParams.put("fromAccount", "2");
        requestParams.put("amount", value);
        requestParams.put("transactionType", "TRANSFER");
        request.header("Content-Type", "application/json");
        request.body(requestParams.toString());
        Response response = request.post("/revolut/transfer");
        int statusCode = response.getStatusCode();
        Assert.assertEquals( 200, statusCode);
    }
}
