package com.revolut.service;

import com.revolut.enums.TransactionType;
import com.revolut.exception.InsufficientBalanceException;
import com.revolut.exception.InvalidAccountException;
import com.revolut.exception.InvalidAmountException;
import com.revolut.model.Account;
import com.revolut.model.Money;
import com.revolut.model.Transaction;
import com.revolut.proxy.BankServiceInitializer;
import com.revolut.store.AccountStore;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicInteger;

public class BankServiceImplTest {

    private BankService bankService;

    @Before
    public void setUp() {
        bankService = BankServiceInitializer.newInstance(new BankServiceImpl());
        Account account = new Account();
        account.setAccountNumber("1");
        account.setBalance(new BigDecimal("100"));
        Account account2 = new Account();
        account2.setAccountNumber("2");
        account2.setBalance(new BigDecimal("100"));
        AccountStore.getAccountMap().put("1", account);
        AccountStore.getAccountMap().put("2", account2);
    }

    @Test
    public void testValidTransactionDeposit() throws InsufficientBalanceException, InvalidAccountException, InvalidAmountException {
        Transaction transaction = new Transaction();
        transaction.setToAccount("1");
        transaction.setTransactionType(TransactionType.DEPOSIT);
        Money money = new Money();
        money.setValue(new BigDecimal("100"));
        transaction.setAmount(money);
        bankService.deposit(transaction);
        Account updatedAccount = AccountStore.getAccountMap().get("1");
        Assert.assertTrue(BigDecimal.valueOf(new Long(200)).compareTo(updatedAccount.getBalance()) == 0);
    }

    @Test(expected = InvalidAmountException.class)
    public void testInvalidTransactionDeposit() throws InsufficientBalanceException, InvalidAccountException, InvalidAmountException {
        Transaction transaction = new Transaction();
        transaction.setToAccount("1");
        transaction.setTransactionType(TransactionType.DEPOSIT);
        Money money = new Money();
        money.setValue(new BigDecimal("0"));
        transaction.setAmount(money);
        bankService.deposit(transaction);
        Account updatedAccount = AccountStore.getAccountMap().get("1");
        Assert.assertTrue(BigDecimal.valueOf(new Long(200)).compareTo(updatedAccount.getBalance()) == 0);
    }

    @Test(expected = InvalidAmountException.class)
    public void testNullTransactionDeposit() throws InsufficientBalanceException, InvalidAccountException, InvalidAmountException {
        Transaction transaction = new Transaction();
        transaction.setToAccount(null);
        transaction.setTransactionType(TransactionType.DEPOSIT);
        Money money = new Money();
        money.setValue(new BigDecimal("0"));
        transaction.setAmount(money);
        bankService.deposit(transaction);
        Account updatedAccount = AccountStore.getAccountMap().get("1");
        Assert.assertTrue(BigDecimal.valueOf(new Long(200)).compareTo(updatedAccount.getBalance()) == 0);
    }

    @Test
    public void testValidTransactionWithdraw() throws InsufficientBalanceException, InvalidAccountException, InvalidAmountException {
        Transaction transaction = new Transaction();
        transaction.setFromAccount("1");
        transaction.setTransactionType(TransactionType.WITHDRAW);
        Money money = new Money();
        money.setValue(new BigDecimal("100"));
        transaction.setAmount(money);
        bankService.withdraw(transaction);
        Account updatedAccount = AccountStore.getAccountMap().get("1");
        Assert.assertTrue(BigDecimal.valueOf(new Long(0)).compareTo(updatedAccount.getBalance()) == 0);
    }

    @Test(expected = InvalidAccountException.class)
    public void testNullAccountransactionWithdraw() throws InsufficientBalanceException, InvalidAccountException, InvalidAmountException {
        Transaction transaction = new Transaction();
        transaction.setFromAccount(null);
        transaction.setTransactionType(TransactionType.WITHDRAW);
        Money money = new Money();
        money.setValue(new BigDecimal("100"));
        transaction.setAmount(money);
        bankService.withdraw(transaction);
        Account updatedAccount = AccountStore.getAccountMap().get("1");
        Assert.assertTrue(BigDecimal.valueOf(new Long(0)).compareTo(updatedAccount.getBalance()) == 0);
    }

    @Test(expected = InsufficientBalanceException.class)
    public void testInvalidTransactionWithdraw() throws InsufficientBalanceException, InvalidAccountException, InvalidAmountException {
        Transaction transaction = new Transaction();
        transaction.setFromAccount("1");
        transaction.setTransactionType(TransactionType.WITHDRAW);
        Money money = new Money();
        money.setValue(new BigDecimal("0"));
        transaction.setAmount(money);
        bankService.withdraw(transaction);
        Account updatedAccount = AccountStore.getAccountMap().get("1");
        Assert.assertTrue(BigDecimal.valueOf(new Long(0)).compareTo(updatedAccount.getBalance()) == 0);
    }

    @Test
    public void testValidTransactionTransfer() throws InsufficientBalanceException, InvalidAccountException, InvalidAmountException {
        Transaction transaction = new Transaction();
        transaction.setFromAccount("1");
        transaction.setToAccount("2");
        transaction.setTransactionType(TransactionType.TRANSFER);
        Money money = new Money();
        money.setValue(new BigDecimal("100"));
        transaction.setAmount(money);
        bankService.transfer(transaction);
        Account updatedAccount = AccountStore.getAccountMap().get("1");
        Account updatedAccount2 = AccountStore.getAccountMap().get("2");
        Assert.assertTrue(BigDecimal.valueOf(new Long(0)).compareTo(updatedAccount.getBalance()) == 0);
        Assert.assertTrue(BigDecimal.valueOf(new Long(200)).compareTo(updatedAccount2.getBalance()) == 0);
    }

    @Test(expected = InvalidAmountException.class)
    public void testInvalidTransactionTransfer() throws InsufficientBalanceException, InvalidAccountException, InvalidAmountException {
        Transaction transaction = new Transaction();
        transaction.setFromAccount("1");
        transaction.setToAccount("2");
        transaction.setTransactionType(TransactionType.TRANSFER);
        Money money = new Money();
        money.setValue(new BigDecimal("0"));
        transaction.setAmount(money);
        bankService.transfer(transaction);
        Account updatedAccount = AccountStore.getAccountMap().get("1");
        Account updatedAccount2 = AccountStore.getAccountMap().get("2");
        Assert.assertTrue(BigDecimal.valueOf(new Long(0)).compareTo(updatedAccount.getBalance()) == 0);
        Assert.assertTrue(BigDecimal.valueOf(new Long(200)).compareTo(updatedAccount2.getBalance()) == 0);
    }

    @Test(expected = InvalidAccountException.class)
    public void testInvalidAccountTransfer() throws InsufficientBalanceException, InvalidAccountException, InvalidAmountException {
        Transaction transaction = new Transaction();
        transaction.setFromAccount(null);
        transaction.setToAccount("2");
        transaction.setTransactionType(TransactionType.TRANSFER);
        Money money = new Money();
        money.setValue(new BigDecimal("10"));
        transaction.setAmount(money);
        bankService.transfer(transaction);
        Account updatedAccount = AccountStore.getAccountMap().get("1");
        Account updatedAccount2 = AccountStore.getAccountMap().get("2");
        Assert.assertTrue(BigDecimal.valueOf(new Long(0)).compareTo(updatedAccount.getBalance()) == 0);
        Assert.assertTrue(BigDecimal.valueOf(new Long(200)).compareTo(updatedAccount2.getBalance()) == 0);
    }

    @Test(expected = InvalidAccountException.class)
    public void testInvalidAccount2Transfer() throws InsufficientBalanceException, InvalidAccountException, InvalidAmountException {
        Transaction transaction = new Transaction();
        transaction.setFromAccount("1");
        transaction.setToAccount(null);
        transaction.setTransactionType(TransactionType.TRANSFER);
        Money money = new Money();
        money.setValue(new BigDecimal("10"));
        transaction.setAmount(money);
        bankService.transfer(transaction);
        Account updatedAccount = AccountStore.getAccountMap().get("1");
        Account updatedAccount2 = AccountStore.getAccountMap().get("2");
        Assert.assertTrue(BigDecimal.valueOf(new Long(0)).compareTo(updatedAccount.getBalance()) == 0);
        Assert.assertTrue(BigDecimal.valueOf(new Long(200)).compareTo(updatedAccount2.getBalance()) == 0);
    }

    @Test(expected = InvalidAccountException.class)
    public void testSameAccountTransfer() throws InsufficientBalanceException, InvalidAccountException, InvalidAmountException {
        Transaction transaction = new Transaction();
        transaction.setFromAccount("1");
        transaction.setToAccount("1");
        transaction.setTransactionType(TransactionType.TRANSFER);
        Money money = new Money();
        money.setValue(new BigDecimal("10"));
        transaction.setAmount(money);
        bankService.transfer(transaction);
        Account updatedAccount = AccountStore.getAccountMap().get("1");
        Account updatedAccount2 = AccountStore.getAccountMap().get("2");
        Assert.assertTrue(BigDecimal.valueOf(new Long(0)).compareTo(updatedAccount.getBalance()) == 0);
        Assert.assertTrue(BigDecimal.valueOf(new Long(200)).compareTo(updatedAccount2.getBalance()) == 0);
    }

    @Test
    public void testCreateAccount() {
        String customerName = "Revolut";
        String accountNumber = bankService.createAccount(customerName);
        Assert.assertEquals("1", accountNumber);
        Assert.assertEquals("1", AccountStore.getAccountMap().get(accountNumber).getAccountNumber());
    }

    @org.testng.annotations.Test(threadPoolSize = 5, invocationCount = 10)
    public void testConcurrentCreateAccount() {
        BankService bankService = BankServiceInitializer.newInstance(new BankServiceImpl());
        String customerName = "Revolut";
        String accountNumber = bankService.createAccount(customerName);
        AtomicInteger count = new AtomicInteger(0);
        int counter = count.incrementAndGet();
        if(counter == 10) {
            Assert.assertEquals(9, AccountStore.getAccountMap().size());
        }
    }
}
