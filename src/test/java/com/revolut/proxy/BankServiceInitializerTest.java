//package com.revolut.proxy;
//
//import com.revolut.enums.Currency;
//import com.revolut.model.Account;
//import com.revolut.model.Money;
//import com.revolut.model.Transaction;
//import com.revolut.service.BankService;
//import com.revolut.service.BankServiceImpl;
//import com.revolut.validation.Validator;
//import com.revolut.validation.ValidatorImpl;
//import org.junit.Before;
//import org.junit.Test;
//
//import java.math.BigDecimal;
//
//public class BankServiceInitializerTest {
//
//    Transaction transaction;
//
//    @Before
//    public void setUp() {
//        transaction = new Transaction();
//        Account account = new Account();
//        Money money = new Money();
//        money.setCurrency(Currency.GBP);
//        money.setValue(new BigDecimal(200));
//        account.setBalance(new BigDecimal(500));
//        account.setAccountNumber("1");
//        account.setCustomer(null);
//        transaction.setFromAccount(account);
//        transaction.setAmount(money);
//    }
//
////    @Test
////    public void testBankInitializer() {
////        BankService obj = BankServiceInitializer.newInstance(new BankServiceImpl());
////        obj.deposit(transaction);
////    }
//}
