# Money Transfer

This is a money transfer api service where in the functionalities exposed are creation of account, deposit, withdraw, transfer. It performs operation in a thread safe manner. Account created are help in a hashmap in memory.

###Technologies and Libraries used
1. Java 8
2. Test ng (Concurrent testing)
3. Rest assured (Testing rest api)
4. Jersey embedded server
5. Apache commons

##Api Specifications

1. POST /createAccount

Request
```
{
"name": "Revolut"
}
```
Response
```
201 CREATED
```

2. POST /deposit

Request
```
{
	"toAccount":"1",
	"amount": {
		"value":"200",
		"currency":"GBP"
	}
}
```

Response

```
200 OK
```
3. POST /withdraw

Request
```
{
	"fromAccount":"1",
	"amount": {
		"value":"200",
		"currency":"GBP"
	}
}
```

Response

```
200 OK
```

2. POST /transfer

Request
```
{
	"toAccount":"1",
	"fromAccount":"2",
	"amount": {
		"value":"200",
		"currency":"GBP"
	}
}
```

Response

```
200 OK
```

##Running Instructions

```
mvn clean install
```

Execute

```
java -jar target/MoneyTransfer-1.0-SNAPSHOT.jar
```